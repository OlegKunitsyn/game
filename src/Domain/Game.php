<?php

namespace App\Domain;

final class Game
{
    public function __construct(private readonly IMoveRepository $moves)
    {
    }

    public function restart(): self
    {
        $this->moves->clear();
        return $this;
    }

    /**
     * @throws GameException
     */
    public function next(Move $move): self
    {
        if (!$this->isValid($move, $this->moves->getLast()) || !$this->moves->isValid($move)) {
            throw new GameException("Invalid move, {$move->player()->name()}.");
        }
        $this->moves->add($move);
        return $this;
    }

    public function getStatus(): Status
    {
        return new Status($this->moves->getCount(), $this->moves->getLast()?->word() ?? null);
    }

    public function getWinner(): Winner
    {
        return new Winner($this->moves->getLast());
    }

    public function isValid(Move $move, ?Move $lastMove): bool
    {
        if (empty($move->word()) || empty($move->player()->name())) {
            return false;
        }
        if (null === $lastMove) {
            return true;
        }
        if (empty($lastMove->word()) || empty($lastMove->player()->name())) {
            return false;
        }
        return strtolower(substr($move->word(), 0, 1)) === strtolower(substr($lastMove->word(), -1, 1));
    }
}
