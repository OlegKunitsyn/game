<?php

namespace App\Domain;

use Stringable;

final class Status implements Stringable
{
    public function __construct(public readonly int $moves, public readonly ?string $lastWord)
    {
    }

    public function __toString(): string
    {
        return "Moves: {$this->moves}, Last word: {$this->lastWord}";
    }
}
