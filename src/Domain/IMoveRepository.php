<?php

namespace App\Domain;

interface IMoveRepository
{
    /**
     * Clear all moves
     */
    public function clear(): void;

    /**
     * Add move
     */
    public function add(Move $move): void;

    /**
     * Validate move
     */
    public function isValid(Move $move): bool;

    /**
     * Get last move if any
     */
    public function getLast(): ?Move;

    /**
     * Total number of moves
     */
    public function getCount(): int;
}
