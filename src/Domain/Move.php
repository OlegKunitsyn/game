<?php

namespace App\Domain;

final class Move
{
    public function __construct(private readonly Player $player, private readonly string $word)
    {
    }

    public function player(): Player
    {
        return $this->player;
    }

    public function word(): string
    {
        return $this->word;
    }
}
