<?php

namespace App\Domain;

use LogicException;
use Throwable;

final class GameException extends LogicException
{
    public readonly string $error;

    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        $this->error = $message;
        parent::__construct($message, $code, $previous);
    }
}
