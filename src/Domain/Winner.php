<?php

namespace App\Domain;

use Stringable;

final class Winner implements Stringable
{
    public readonly ?string $player;
    public readonly ?string $word;

    public function __construct(?Move $lastMove)
    {
        $this->player = $lastMove?->player()->name();
        $this->word = $lastMove?->word();
    }

    public function __toString(): string
    {
        return "Word '{$this->word}' by {$this->player}";
    }
}
