<?php

namespace App\Application\Controller;

use App\Domain\Game;
use App\Domain\Move;
use App\Domain\Player;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/v1/game")]
class GameController extends AbstractController
{
    public function __construct(private readonly Game $game)
    {
    }

    #[Route('/', methods: ['GET'])]
    public function status(): Response
    {
        return $this->json($this->game->getStatus());
    }

    #[Route('/', methods: ['DELETE'])]
    public function restart(): Response
    {
        $status = $this->game
            ->restart()
            ->getStatus();
        return $this->json($status);
    }

    #[Route('/winner', methods: ['GET'])]
    public function winner(): Response
    {
        return $this->json($this->game->getWinner());
    }

    #[Route('/move/{player}/{word}', methods: ['POST'])]
    public function move(string $player, string $word): Response
    {
        $status = $this->game
            ->next(new Move(new Player($player), $word))
            ->getStatus();
        return $this->json($status);
    }
}
