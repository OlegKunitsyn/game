<?php

namespace App\Application\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use App\Infrastructure\Repository\DictionaryRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import')]
final class DictionaryCommand extends Command
{
    public function __construct(private readonly DictionaryRepository $repository)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('Run or safely re-run this command')
            ->addArgument('file', InputArgument::REQUIRED, 'Path to a file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!file_exists($input->getArgument('file'))) {
            $output->writeln("No file found");
            return Command::FAILURE;
        }

        $handle = fopen($input->getArgument('file'), 'r');
        while (!feof($handle)) {
            $word = trim(fgets($handle));
            if (!empty($word)) {
                $this->repository->add($word);
                $output->writeln($word);
            }
        }
        fclose($handle);

        $output->writeln('Done');
        return Command::SUCCESS;
    }
}
