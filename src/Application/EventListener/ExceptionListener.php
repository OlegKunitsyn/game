<?php

namespace App\Application\EventListener;

use App\Domain\GameException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof HttpExceptionInterface) {
            $event->setResponse(new Response($exception->getMessage(), Response::HTTP_BAD_REQUEST));
        }
        if ($exception instanceof GameException) {
            $event->setResponse(new JsonResponse($exception, Response::HTTP_BAD_REQUEST));
        }
    }
}
