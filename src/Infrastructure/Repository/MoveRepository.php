<?php

namespace App\Infrastructure\Repository;

use App\Domain\IMoveRepository;
use App\Domain\Move;

class MoveRepository implements IMoveRepository
{
    private const LAST_MOVE_KEY = '_';
    private const COUNT_KEY = '__';

    public function __construct(private readonly DictionaryRepository $dictionary)
    {
    }

    public function clear(): void
    {
        apcu_clear_cache();
    }

    public function add(Move $move): void
    {
        apcu_store($move->word(), $move->player()->name());
        apcu_store(self::LAST_MOVE_KEY, serialize($move));
        apcu_inc(self::COUNT_KEY);
    }

    public function isValid(Move $move): bool
    {
        // not in dictionary?
        if (!$this->dictionary->exists($move->word())) {
            return false;
        }

        // already submitted?
        return !apcu_exists($move->word());
    }

    public function getLast(): ?Move
    {
        if ($lastMove = apcu_fetch(self::LAST_MOVE_KEY)) {
            return unserialize($lastMove);
        }
        return null;
    }

    public function getCount(): int
    {
        if ($count = apcu_fetch(self::COUNT_KEY)) {
            return $count;
        }
        return 0;
    }
}
