<?php

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Dictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Dictionary>
 *
 * @method Dictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dictionary[]    findAll()
 * @method Dictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DictionaryRepository extends ServiceEntityRepository
{
    private ManagerRegistry $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
        parent::__construct($registry, Dictionary::class);
    }

    /**
     * Atomic seeding
     */
    public function add(string $word): void
    {
        try {
            $entity = new Dictionary();
            $entity->setWord($word);
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();
        } catch (UniqueConstraintViolationException $ignored) {
            $this->registry->resetManager();
        }
    }

    /**
     * Check presence
     */
    public function exists(string $word): bool
    {
        return null !== $this->findOneBy(['word' => $word]);
    }
}
