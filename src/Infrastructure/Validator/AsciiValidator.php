<?php

namespace App\Infrastructure\Validator;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AsciiValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Ascii) {
            throw new UnexpectedTypeException($constraint, Ascii::class);
        }

        if (false === @iconv('UTF-8', 'ASCII', $value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
