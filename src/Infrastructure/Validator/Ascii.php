<?php

namespace App\Infrastructure\Validator;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class Ascii extends Constraint
{
    public string $message = 'Non-ASCII charset found';
}
