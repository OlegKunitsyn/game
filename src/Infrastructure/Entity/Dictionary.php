<?php

namespace App\Infrastructure\Entity;

use App\Infrastructure\Repository\DictionaryRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Validator as WordAssert;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DictionaryRepository::class)]
#[ORM\UniqueConstraint(name: "word", columns: ["word"])]
class Dictionary
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private ?int $id;

    #[ORM\Column(type: "string", length: 32, nullable: false)]
    #[Assert\Length(max: 32)]
    #[Assert\NotBlank]
    #[WordAssert\Ascii]
    private ?string $word;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;
        return $this;
    }
}
