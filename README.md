# Backend Challenge :rocket:
Welcome to the coding challenge :wave:

## Your challenge

### User Story
As someone who likes games, I want to have a little game which I can play with a friend by taking turns at the keyboard, so that we can amuse ourselves.

### Description
What game you choose and how the users interface with the game (CLI, a webpage, etc) is up to you. Our strong suggestion: Keep it simple. Some games you might consider are tic-tac-toe, connect 4, Hangman, rock-paper-scissors - we're not looking for Doom or Minecraft here.

We don't expect you to spend too much of your free time on this project - aim for maximum 3-4 hours.

After you submit your pull request, our team will review it within two working days. You are invited to participate in the discussion: we want you to see how it would be to work with us! :raised_hands:

### Requirements
* Use PHP with an object-oriented approach
* Create a feature branch
* Create a simple two-player game which the players can play by taking turns at the keyboard
* After a user has made a move, display the current state of the game
* Check if the user inputs a valid move
* Show a message if the game has been won
* Offer a way to start a new game
* Add a section to the README.md explaining how we can run the project
* Also add an explanation of (or link to) the rules of the game, in case it's one we don't know
* Create your pull request on github and let us know you're finished via email!

### What we explicitly don't care about
* This is not a frontend task. You do not need to make this pretty in any form

### Hints
* Use the best practices of PHP as you understand them
* Commit often and write meaningful commit messages
* If you are using a framework, please do the framework commits separate from your own code
* Consider extensibility. We might need to add a feature later
* Be prepared to walk us through the thought process of how you organized and executed the challenge

### For overachievers
* We use docker
* We write unit tests
* We use the newest PHP version when possible

## Your results

### Solution
This is a multi-player game called "Words" where each player submits a noun which begins with the last char of the previously submitted noun. 
Single-player mode is possible as well. The list of English nouns includes 6.7K words, see `assets`.

- PHP 8.1, Symfony 6
- PSR-12
- DDD
- SQLite ORM, APCU in-memory
- REST, CLI
- curl is a keyboard client for geeks

### Limitations
- no user roles, no AAA

### API
- `GET /v1/game/` status of the game
- `DELETE /v1/game/` restart of the game
- `GET /v1/game/winner` the last move always wins
- `POST /v1/game/move/{player}/{word}` next move, simple without payload

### Preventive controls for CI
Pushed changesets are tested automatically by GitLab CI/CD.
```
$ php bin/phplint.phar src
$ php bin/phpcs.phar --standard=PSR12 --extensions=php -n src -n tests
$ php bin/console lint:yaml config
$ vendor/bin/phpstan analyse src tests
$ php bin/phpunit
```

### Development
Set up:
```
$ composer install
$ php bin/console doctrine:schema:create
$ php bin/console app:import assets/dictionary.txt
```

#### Launch via host
```
$ symfony server:start
# no symfony? Try php built-in server
# php -S localhost:8000 -t public
$ curl -X GET http://localhost:8000/v1/game/winner
{"player":null,"word":null}
```

#### Launch via Docker container
```
$ docker-compose build
$ docker-compose up
$ curl -X GET http://localhost:8000/v1/game/winner
{"player":null,"word":null}
```

### Usage example
```
$ curl -X GET http://localhost:8000/v1/game/winner
{"player":null,"word":null}
$ curl -X POST http://localhost:8000/v1/game/move/player1/wind
{"moves":1,"lastWord":"wind"}
$ curl -X POST http://localhost:8000/v1/game/move/player2/duck
{"moves":2,"lastWord":"duck"}
$ curl -X POST http://localhost:8000/v1/game/move/player2/kind
{"moves":3,"lastWord":"kind"}
$ curl -X POST http://localhost:8000/v1/game/move/player1/duck
{"error":"Invalid move, player1."}
$ curl -X GET http://localhost:8000/v1/game/winner
{"player":"player2","word":"kind"}
$ curl -X DELETE http://localhost:8000/v1/game/
{"moves":0,"lastWord":null}
```

### Progress report
* ~~Use PHP with an object-oriented approach~~
* ~~Create a feature branch~~
* ~~Create a simple two-player game which the players can play by taking turns at the keyboard~~
* ~~After a user has made a move, display the current state of the game~~
* ~~Check if the user inputs a valid move~~
* ~~Show a message if the game has been won~~
* ~~Offer a way to start a new game~~
* ~~Add a section to the README.md explaining how we can run the project~~
* ~~Also add an explanation of (or link to) the rules of the game, in case it's one we don't know~~
* Create your pull request on github and let us know you're finished via email!
* ~~This is not a frontend task. You do not need to make this pretty in any form~~
* ~~Use the best practices of PHP as you understand them~~
* ~~Commit often and write meaningful commit messages~~
* ~~If you are using a framework, please do the framework commits separate from your own code~~
* ~~Consider extensibility. We might need to add a feature later~~
* ~~Be prepared to walk us through the thought process of how you organized and executed the challenge~~
* ~~We use docker~~
* ~~We write unit tests~~
* ~~We use the newest PHP version when possible~~

### Deployment
Provisioning powered by Hetzner Cloud and OVH DNS.

#### Secrets
- Generate RSA key-pair in OpenSSH format:
```
$ cd deployment/test
$ ssh-keygen -o -b 4096 -t rsa -f rsa
```
- Upload public key to `https://console.hetzner.cloud`.
- Create OVH credentials on `https://api.ovh.com/createToken/?GET=/*&POST=/*&PUT=/*&DELETE=/*`
- Add variables in `Settings -> GitLab CI/CD` 
  - `HETZNER_TOKEN` value
  - `HETZNER_FINGERPRINT` value
  - `SSH_PRIVATE_KEY` value
  - `OVH_APP_KEY` value
  - `OVH_APP_SECRET` value
  - `OVH_CONSUMER_KEY` value

#### Publish Docker container
```
$ docker login registry.gitlab.com
$ docker build -t registry.gitlab.com/olegkunitsyn/game .
$ docker push registry.gitlab.com/olegkunitsyn/game
```

#### Test environment
Automated by GitLab CI/CD pipelines:
- pick an untagged change-set
- await `setup` action, which will
- create a `CX11` `Debian 11` node and DNS records,
- deploy the change-set at `<branch>@<domain.name>` in `test` (.env) mode and
- await `teardown` action, which will
- destroy associated resources.
