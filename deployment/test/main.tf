# Providers
terraform {
  backend "http" {
  }
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
    ovh = {
      source = "ovh/ovh"
    }
  }
}
provider "ovh" {
  endpoint           = "ovh-eu"
  application_key    = var.ovh_application_key
  application_secret = var.ovh_application_secret
  consumer_key       = var.ovh_consumer_key
}
provider "hcloud" {
  token = var.hetzner_token
}

# Variables
locals {
  domain_zone = "testdomain.ovh"
  env         = "test"
  project_dir = "/var/www/game"
  repository  = "https://gitlab.com/OlegKunitsyn/game.git"
}
variable "branch" {
  type    = string
  default = "develop"
}
variable "hetzner_token" {
  type = string
}
variable "hetzner_fingerprint" {
  type = string
}
data "hcloud_ssh_key" "ssh_key" {
  fingerprint = var.hetzner_fingerprint
}
variable "ovh_application_key" {
  type = string
}
variable "ovh_application_secret" {
  type = string
}
variable "ovh_consumer_key" {
  type = string
}
variable "ssh_private_key" {
  type = string
}

# Boot
data "hcloud_ssh_keys" "all" {
}
resource "hcloud_server" "node" {
  name        = "${var.branch}.${local.domain_zone}"
  image       = "debian-11"
  server_type = "cx11"
  datacenter  = "fsn1-dc14"
  ssh_keys    = data.hcloud_ssh_keys.all.ssh_keys.*.name
}

# DNS
resource "ovh_domain_zone_record" "ipv4" {
  zone      = local.domain_zone
  subdomain = var.branch
  target    = hcloud_server.node.ipv4_address
  fieldtype = "A"
  ttl       = "3600"
}
resource "ovh_domain_zone_record" "ipv6" {
  zone      = local.domain_zone
  subdomain = var.branch
  target    = hcloud_server.node.ipv6_address
  fieldtype = "AAAA"
  ttl       = "3600"
}

# Setup
resource "null_resource" "setup" {
  connection {
    type        = "ssh"
    user        = "root"
    host        = hcloud_server.node.ipv4_address
    private_key = fileexists(var.ssh_private_key) ? file(var.ssh_private_key) : var.ssh_private_key
  }
  provisioner "remote-exec" {
    inline = [
      "apt-get update",
      "apt-get -y install lsb-release apt-transport-https ca-certificates wget",
      "wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg",
      "echo \"deb https://packages.sury.org/php/ $(lsb_release -sc) main\" | tee /etc/apt/sources.list.d/php.list",
      "apt-get update",
      "apt-get -y install git unzip mc net-tools php8.1 php8.1-fpm php8.1-sqlite3 php8.1-intl php8.1-mbstring php8.1-curl php8.1-iconv php8.1-ctype php8.1-dom php8.1-apcu php8.1-opcache fail2ban",
      "systemctl enable fail2ban",
      "systemctl start fail2ban",
      "systemctl enable php8.1-fpm",
      "apt-get -y install -y debian-keyring debian-archive-keyring apt-transport-https gnupg",
      "curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --yes --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg",
      "curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list",
      "apt-get update",
      "apt-get -y install caddy",
      "systemctl enable caddy",
    ]
  }
  provisioner "file" {
    content = templatefile("Caddyfile", {
      domain_name = "${var.branch}.${local.domain_zone}"
      root        = "${local.project_dir}/public"
    })
    destination = "/etc/caddy/Caddyfile"
  }
  provisioner "remote-exec" {
    inline = [
      "mkdir /var/www",
      "cd /var/www",
      "git clone ${local.repository} -b ${var.branch}",
      "cd ${local.project_dir}",
      "php bin/composer.phar install --optimize-autoloader --no-interaction",
      "chown www-data:www-data ${local.project_dir}/var",
      "sudo -u www-data php bin/console doctrine:schema:create",
      "sudo -u www-data php bin/console app:import assets/dictionary.txt",
      "systemctl restart php8.1-fpm",
      "systemctl restart caddy",
      "sleep 1"
    ]
  }
}

# Finish
output "public_ip" {
  description = "Public IP"
  value       = hcloud_server.node.ipv4_address
}
output "hostname" {
  description = "Hostname"
  value       = "${var.branch}.${local.domain_zone}"
}
output "directory" {
  description = "Project directory"
  value       = local.project_dir
}
output "env" {
  description = "Application environment"
  value       = local.env
}
output "branch" {
  description = "Repository branch"
  value       = var.branch
}
output "status" {
  description = "Server status"
  value       = hcloud_server.node.status
}
