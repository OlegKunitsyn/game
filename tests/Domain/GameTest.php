<?php

namespace App\Tests\Domain;

use App\Domain\Game;
use App\Domain\GameException;
use App\Domain\Move;
use App\Domain\Player;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    private Game $game;

    public function setUp(): void
    {
        $this->game = new Game(new FictionalMoveRepository());
    }

    public function testStatus(): void
    {
        $test = $this->game
            ->next(new Move(new Player('John'), 'test'))
            ->getStatus();
        static::assertEquals('test', $test->lastWord);
        static::assertEquals(1, $test->moves);
    }

    public function testWinner(): void
    {
        $test = $this->game->getWinner();
        static::assertNull($test->player);
        static::assertNull($test->word);

        $test = $this->game
            ->next(new Move(new Player('John'), 'test'))
            ->getWinner();
        static::assertEquals('John', $test->player);
        static::assertEquals('test', $test->word);
    }

    public function testRestart(): void
    {
        $test = $this->game->getStatus();
        static::assertNull($test->lastWord);
        static::assertEquals(0, $test->moves);

        $test = $this->game
            ->next(new Move(new Player('John'), 'test'))
            ->getStatus();
        static::assertEquals('test', $test->lastWord);
        static::assertEquals(1, $test->moves);

        $test = $this->game
            ->restart()
            ->getStatus();
        static::assertEquals(null, $test->lastWord);
        static::assertEquals(0, $test->moves);
    }

    public function testIsValidFirstMove(): void
    {
        static::assertTrue(
            $this->game->isValid(
                new Move(new Player('John'), 'test'),
                null
            )
        );
    }

    public function testIsValid(): void
    {
        static::assertTrue(
            $this->game->isValid(
                new Move(new Player('John'), 'Abc'),
                new Move(new Player('Smith'), 'cba')
            )
        );
        static::assertFalse(
            $this->game->isValid(
                new Move(new Player('John'), 'abc'),
                new Move(new Player('Smith'), 'abc')
            )
        );
        static::assertFalse(
            $this->game->isValid(
                new Move(new Player('John'), 'abc'),
                new Move(new Player('Smith'), '')
            )
        );
        static::assertFalse(
            $this->game->isValid(
                new Move(new Player('John'), ''),
                new Move(new Player('Smith'), 'abc')
            )
        );
    }

    public function testNextExceptionEmptyPlayer(): void
    {
        static::expectException(GameException::class);
        $this->game->next(new Move(new Player(''), 'test'));
    }

    public function testNextExceptionEmptyWord(): void
    {
        static::expectException(GameException::class);
        $this->game->next(new Move(new Player('player'), ''));
    }
}
