<?php

namespace App\Tests\Domain;

use App\Domain\IMoveRepository;
use App\Domain\Move;

final class FictionalMoveRepository implements IMoveRepository
{
    private ?Move $lastMove = null;
    private int $moves = 0;

    public function clear(): void
    {
        $this->moves = 0;
        $this->lastMove = null;
    }

    public function add(Move $move): void
    {
        $this->lastMove = $move;
        $this->moves++;
    }

    public function isValid(Move $move): bool
    {
        return true;
    }

    public function getLast(): ?Move
    {
        return $this->lastMove;
    }

    public function getCount(): int
    {
        return $this->moves;
    }
}
